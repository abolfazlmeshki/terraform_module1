output "bucket_arn" {
  value = aws_s3_bucket.meshki_bucket.arn
  description = "Show S3 bucket name"
}