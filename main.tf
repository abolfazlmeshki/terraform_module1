provider "aws" {
  region         = var.region
}

resource "aws_s3_bucket" "meshki_bucket" {
  
}

resource "aws_s3_bucket_acl" "acl" {
  acl = "private" 
  bucket = aws_s3_bucket.meshki_bucket.bucket
}